#!/usr/bin/env zsh

echo "Checking for rust"

if which cargo >/dev/null 2>&1; then
	echo "  found cargo"
	if [[ ! "$(rustup toolchain list)" =~ "stable" ]]; then
		echo "  installing stable toolchain"
		rustup update stable
	fi
else
	if which rustup >/dev/null 2>&1; then
		echo "  foud rustup"
		echo "  installing stable toolchain"
		rustup update stable
	else
		echo "  installing rustup"
		curl --proto "=https" --tlsv1.2 -sSf https://sh.rustup.rs | sh
	fi
	source "$HOME/.cargo/env"
fi

wanted_crates=(bat lsd cargo-update topgrade)
crates=()

if [ -n "$DISPLAY" ]; then
	wanted_packages+=(
		notification-server)
fi

echo "Checking for installed crates"
installed_crates=$(cargo install --list)
for c in $wanted_crates; do
	if ! echo "${installed_crates}" | grep -e "^${c}" >/dev/null; then
		crates+=$c
	fi
done

if [[ ${#crates} -eq 0 ]]; then
	echo "  All crates installed"
else
	echo "Installing cargo crates"
	cargo install ${crates[@]}
fi

