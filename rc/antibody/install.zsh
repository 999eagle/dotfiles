#!/usr/bin/env zsh

echo "Installing plugins with antibody"

# Install antibody if not installed
if ! which antibody >/dev/null 2>&1; then
	if [ ! -d $ANTIBODY_INSTALL_DIR ]; then mkdir -p $ANTIBODY_INSTALL_DIR; fi
	curl -sfL https://git.io/antibody | sh -s - -b "$ANTIBODY_INSTALL_DIR"
fi

# Generate antibody script
$ANTIBODY_INSTALL_DIR/antibody bundle <$DOTFILES/rc/antibody/zsh_plugins.txt >$DOTFILES/local/antibody_plugins.zsh
