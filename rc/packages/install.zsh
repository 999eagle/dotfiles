#!/usr/bin/env zsh

echo "Checking for required packages"

wanted_packages=(tmux powerline thefuck)
packages=()
pkg_mgr=''
pkg_mgr_bin=''

if [ -n "$DISPLAY" ]; then
	wanted_packages+=(
		xorg-xinput xgetres xplugd-git xdotool xorg-xinit
		i3-gaps picom rofi zenity
		betterlockscreen xss-lock
		polybar pasystray network-manager-applet

		wayland qt5-wayland qt6-wayland xorg-xwayland xorg-xlsclients
		sway wofi xdg-desktop-portal-gtk xdg-desktop-portal-wlr
		swayidle swaylock-effects
		waybar network-manager-applet
		swaync clipman
		jq grim slurp

		qt5ct qt6ct kvantum kvantum-theme-arc arc-gtk-theme papirus-icon-theme breeze
		qt5-tools python-dbus playerctl brightnessctl mold)
fi

if which paru >/dev/null 2>&1; then
	pkg_mgr_bin='paru'
	pkg_mgr='pacman'
elif which yay >/dev/null 2>&1; then
	pkg_mgr_bin='yay'
	pkg_mgr='pacman'
elif which pacman >/dev/null 2>&1; then
	pkg_mgr='pacman'
elif which apt >/dev/null 2>&1; then
	pkg_mgr='apt'
fi

if [ -z "$pkg_mgr_bin" ]; then
	pkg_mgr_bin="$pkg_mgr"
fi

case $pkg_mgr in
	pacman)
		wanted_packages+=(rustup gcc pkgconf)
		;;
	apt)
		wanted_packages+=(gcc gcc-multilib pkg-config libssl-dev)
		;;
esac

case $pkg_mgr in
	pacman)
		for p in $wanted_packages; do
			if ! "$pkg_mgr_bin" -Qq $p >/dev/null; then
				packages+=$p
			fi
		done
		;;
	apt)
		for p in $wanted_packages; do
			if [ $(dpkg-query -W -f='${Status}' $p 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
				packages+=$p
			fi
		done
		;;
	*)
		echo "  Please manually install ${wanted_packages[@]}"
		exit 0
		;;
esac

if [[ ${#packages} -eq 0 ]]; then
	echo "  All packages installed"
else
	case $pkg_mgr in
		pacman)
			if [[ "$pkg_mgr_bin" != "pacman" ]]; then
				# assume it's an AUR helper and doesn't use root
				"$pkg_mgr_bin" -S ${packages[@]}
			else
				sudo "$pkg_mgr_bin" -S ${packages[@]}
			fi
			;;
		apt)
			sudo apt-get install ${packages[@]}
			;;
	esac
fi
