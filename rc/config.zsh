#!/usr/bin/env zsh

# Set antibody paths
export ANTIBODY_HOME=$DOTFILES/local/antibody
export ANTIBODY_INSTALL_DIR=$DOTFILES/local/bin

# Set fonts path
export FONT_PATH=$HOME/.local/share/fonts
