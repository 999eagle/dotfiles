#!/usr/bin/env zsh

root=$(dirname "$0")

files=('/etc/systemd/logind.conf')
if [[ -d '/etc/systemd/logind.conf.d' ]]; then
	files+=($(ls /etc/systemd/logind.conf.d/*.conf))
fi

for f in "${files[@]}"; do
	nautovts_new=$(sed -Ene '/^NAutoVTs=/ { s/^NAutoVTs=([[:digit:]]+)([[:space:]]*#.*)?$/\1/; p }' "$f")
	if [[ -n "$nautovts_new" ]]; then nautovts="$nautovts_new"; fi
done
if [[ -n "$nautovts" && $nautovts -lt 6 ]]; then
	echo "NAutoVTs should be set to (at least) the default value of 6".
fi

function check-file {
	src="$1"
	dest="/$1"
	if ! diff -q "$src" "$dest" >/dev/null; then
		while true; do
			echo -n "File $dest might be wrong. Fix it? [N/y/s] "
			read answer
			case $answer in
				'' | 'N' | 'n' )
					break
					;;
				'Y' | 'y' )
					echo "Fixing file $dest"
					cat "$src" | sudo tee "$dest" >/dev/null
					if [[ "$(stat -c '%A' "$src")" == *x*x*x* ]]; then
						sudo chmod +x "$dest"
					fi
					break
					;;
				'S' | 's' )
					echo "Symlinking file $dest"
					sudo ln -sf "$src" "$dest"
					break
					;;
				* )
					echo "Invalid response, continuing"
					;;
			esac
		done
	fi
}

pushd "$root"
for f in **/*; do
	if [[ ! -f "$f" || "$f" = "install.zsh" ]]; then continue; fi
	check-file "$f"
done
popd
