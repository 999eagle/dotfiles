#!/usr/bin/env zsh

function install_fonts() {
	echo "Installing fonts"

	echo "  Downloading Fira Code Nerd Fonts"
	mkdir -p $DOTFILES/local/fonts/FiraCodeNerd
	for i in Light Medium Regular Bold Retina; do
		curl -SsLo $DOTFILES/local/fonts/FiraCodeNerd/FiraCodeNerd-$i.ttf https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/FiraCode/$i/complete/Fira%20Code%20$i%20Nerd%20Font%20Complete.ttf
	done

	echo "  Downloading PowerlineSymbols"
	curl -SsLo $DOTFILES/local/fonts/PowerlineSymbols.otf https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf

	echo "  Copying local fonts"
	cp $DOTFILES/rc/fonts/font_files/* $DOTFILES/local/fonts/

	echo "  Making links"
	mkdir -p $FONT_PATH
	ln -sf $DOTFILES/awesome-terminal-fonts/build/*.ttf $FONT_PATH
	if [[ ! -e $FONT_PATH/dotfile_fonts || ! -d $FONT_PATH/dotfile_fonts ]]; then
		rm -f $FONT_PATH/dotfile_fonts
		ln -sf $DOTFILES/local/fonts $FONT_PATH/dotfile_fonts
	fi

	echo "  Updating font cache"
	fc-cache -fv $FONT_PATH
}

if which fc-cache >/dev/null 2>&1; then
	install_fonts
fi

echo "Installing Nerd Fonts scripts"

mkdir -p $DOTFILES/local/bin/nerd-fonts
for i in all dev fa fae iec linux material oct ple pom seti weather; do
	curl -SsLo $DOTFILES/local/bin/nerd-fonts/i_$i.sh https://github.com/ryanoasis/nerd-fonts/raw/master/bin/scripts/lib/i_$i.sh
done
