: ${ZSH_TMUX_AUTOSTART:=1}
: ${ZSH_TMUX_AUTOQUIT:=$ZSH_TMUX_AUTOSTART}

function update_env_from_tmux() {
	if [ -n "${TMUX}" ]; then
		eval "$(tmux show-env -s)"
	fi
}

if [[ $(tput colors) == 256 ]]; then
	export ZSH_TMUX_TERM=tmux-256color
else
	export ZSH_TMUX_TERM=tmux
fi

if [[ -z "$SSH_CONNECTION" ]]; then
	: ${ZSH_TMUX_SESSION_NAME:=zsh_tmux_${XDG_VTNR}}
else
	: ${ZSH_TMUX_SESSION_NAME:=ssh_tmux}
fi

export ZSH_TMUX_SESSION_NAME

if [[ -z "$TMUX" && "$ZSH_TMUX_AUTOSTART" == "1" ]]; then
	# kitty sets $TERMINFO to its own directory which doesn't contain the tmux terminfo
	if [[ "$TERM" == "xterm-kitty" ]]; then
		unset TERMINFO
		export TERMINFO
	fi
	if [[ "$ZSH_TMUX_AUTOQUIT" == "1" ]]; then
		exec command tmux new-session -As $ZSH_TMUX_SESSION_NAME
	else
		command tmux new-session -As $ZSH_TMUX_SESSION_NAME
	fi
fi

if [ -n "${TMUX}" ]; then
	autoload -U add-zsh-hook
	add-zsh-hook precmd update_env_from_tmux
fi
