#!/usr/bin/env bash

print_cmds() {
	echo "logout"
	echo "suspend"
	echo "hibernate"
	echo "shutdown"
	echo "reboot"
	echo "cancel"
}

if [[ $# -eq 0 ]]; then
	print_cmds
	exit 0
fi

case $1 in
	logout)
		case $XDG_SESSION_DESKTOP in
			KDE)
				qdbus org.kde.ksmserver /KSMServer logout 0 3 3
				;;
			*)
				i3-msg "exit"
				;;
		esac
		;;
	suspend)
		systemctl suspend
		;;
	hibernate)
		systemctl hibernate
		;;
	shutdown)
		systemctl poweroff
		;;
	reboot)
		systemctl reboot
		;;
	cancel)
		# do nothing
		;;
	*)
		print_cmds
		;;
esac
