function out_dir {
	echo ${XDG_SCREENSHOTS_DIR:-${XDG_PICTURES_DIR:-$HOME}}
}
function usage {
	echo "usage: $0 [-n|--notify] (usage|area|window|screen|output)"
}

notify=false
target=''

while [[ "$#" -ge 1 ]]; do
	case "$1" in
		-n|--notify)
			notify=true
			shift
			;;
		area|window|screen|output)
			target=$1
			shift
			;;
		usage)
			usage
			exit 0
			;;
		*)
			echo "unknown arg: $1"
			usage
			exit 1
			;;
	esac
done

if [[ -z "$target" ]]; then
	echo "no target given"
	usage
	exit 1
fi

case "$target" in
	area)
		geometry=$(slurp -d)
		output=''
		if [[ -z "$geometry" ]]; then
			echo "cancelled"
			exit 0
		fi
		;;
	window)
		geometry=$(swaymsg -t get_tree | jq -j '.. | select(.type?) | select(.focused).rect | "\(.x),\(.y) \(.width)x\(.height)"')
		output=''
		;;
	screen)
		geometry=''
		output=$(swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name')
		;;
	output)
		geometry=''
		output=''
		;;
esac

out="$(out_dir)/Screenshot_$(date '+%4Y%m%d_%H%M%S').png"
if [[ -n "$geometry" ]]; then
	args=("-g" "$geometry")
elif [[ -n "$output" ]]; then
	args=("-o" "$output")
fi
if ! grim "${args[@]}" "$out"; then
	if [[ "$notify" == "true" ]]; then
		notify-send -u critical "screenshot" "failed to capture a screenshot"
	else
		echo "failed to capture a screenshot"
	fi
else
	if [[ "$notify" == "true" ]]; then
		notify-send "screenshot" "screenshot saved to $out"
	else
		echo "screenshot saved to $out"
	fi
fi
