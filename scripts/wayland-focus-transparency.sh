#!/usr/bin/env bash

non_focus_opacity=0.8
last_focus=''

declare -A ignored_containers
function should_ignore_container {
	if [[ "$app_id" == "firefox" && $(grep -Ee '- Invidious — Mozilla Firefox$' <<< $title >/dev/null; echo $?) == "0" ]]; then
		return 0
	fi

	return 1
}

swaymsg -m -t SUBSCRIBE '["window"]' | \
jq --unbuffered --compact-output 'select(.change | contains("new") or contains("focus") or contains("title"))' | \
while read -r event; do
	change=$(echo "$event" | jq -r '.change')
	id=$(echo "$event" | jq -r '.container.id')
	title=$(echo "$event" | jq -r '.container.name')
	app_id=$(echo "$event" | jq -r '.container.app_id')
	focused=$(echo "$event" | jq -r '.container.focused')

	case "$change" in
		new)
			swaymsg "[con_id=$id] opacity set $non_focus_opacity"
			;;
		title)
			if should_ignore_container; then
				# container must always be opaque
				swaymsg "[con_id=$id] opacity set 1"
				if [[ "$focused" == "true" ]]; then
					last_focus=''
				fi
			else
				# container should use focus rules for opacity
				if [[ "$focused" == "true" ]]; then
					last_focus=$id
				else
					swaymsg "[con_id=$id] opacity set $non_focus_opacity"
				fi
			fi
			;;
		focus)
			if [[ -n "$last_focus" ]]; then
				swaymsg "[con_id=$last_focus] opacity set $non_focus_opacity"
			fi
			swaymsg "[con_id=$id] opacity set 1"
			# only set the now focused container as last container if it's not an ignored container
			if ! should_ignore_container; then
				last_focus=$id
			fi
			;;
	esac
done
