#!/usr/bin/env bash

if [ -f "$DOTFILES_HOST/wayland-outputs.sh" ]; then
	"$DOTFILES_HOST/wayland-outputs.sh"
fi
if [ -f "$DOTFILES_HOST/wayland-inputs.sh" ]; then
	"$DOTFILES_HOST/wayland-inputs.sh"
fi

wallpaper=$DOTFILES/wallpaper/archlinux.png
if [ -f "$DOTFILES_HOST/wallpaper.png" ]; then
	wallpaper="$DOTFILES_HOST/wallpaper.png"
fi

swaymsg "output * bg $wallpaper fill"
