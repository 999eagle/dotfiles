#!/usr/bin/env bash

if [ -f "$DOTFILES_HOST/autorun.sh" ]; then
	"$DOTFILES_HOST/autorun.sh"
fi

(kitty &)
(firefox &)
