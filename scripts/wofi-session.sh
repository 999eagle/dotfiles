#!/usr/bin/env bash

print_cmds() {
	echo "logout"
	echo "suspend"
	echo "hibernate"
	echo "shutdown"
	echo "reboot"
	echo "cancel"
}

action=$(print_cmds | wofi -d)

case $action in
	logout)
		swaymsg exit
		;;
	suspend)
		systemctl suspend
		;;
	hibernate)
		systemctl hibernate
		;;
	shutdown)
		systemctl poweroff
		;;
	reboot)
		systemctl reboot
		;;
	cancel)
		# do nothing
		;;
esac
