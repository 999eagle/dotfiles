#!/usr/bin/env zsh

echo "Running local install scripts"
source $DOTFILES/rc/config.zsh
for f in $DOTFILES/rc/*/install.zsh; do
	source $f
done
