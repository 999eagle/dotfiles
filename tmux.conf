set -g default-terminal $ZSH_TMUX_TERM

# remap prefix from C-b to C-a
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix

# switch panes with M-arrow without prefix
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# clear pane and history
bind -n C-l send-keys -R C-l \; clear-history

# count windows and panes from 1 instead of 0
set -g base-index 1
setw -g pane-base-index 1

# renumber windows with M-r
bind-key M-r move-window -r

# enable mouse mode
set -g mouse on

# powerline!
run-shell "powerline-daemon -q"
source "/usr/share/powerline/bindings/tmux/powerline.conf"

# plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-sidebar'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'

# plugin config
#set -g @sidebar-tree-command 'tree -C'
set -g @continuum-restore 'on'
set -g @resurrect-capture-pane-contents 'on'
set -g @resurrect-processes 'gotop nvtop'

# initialize tmux plugin manager
run '~/.tmux/plugins/tpm/tpm'
