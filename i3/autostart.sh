#!/usr/bin/env bash

source ~/.config/i3/workspaces

i3-msg "workspace number \"$ws1\"; exec kitty"
i3-msg "workspace number \"$ws3\"; layout tabbed; exec (firefox &) && sleep 20 && $DOTFILES/bin/fix-firefox"

hostname="$(uname -n)"

for f in ~/.config/i3/$hostname/*.sh; do
	"$f"
done
for f in ~/.config/i3/local/*.sh; do
	"$f"
done

i3-msg "workspace number \"$ws1\""

if [[ "$XDG_SESSION_DESKTOP" != "KDE" ]]; then
	(pasystray &)
	(nm-applet &)
	(blueman-applet &)
	(udiskie &)

	(krunner --replace --daemon &)
fi

(keepassxc &)
