#!/usr/bin/env bash

(nextcloud --background &)
(ckb-next --background &)
(cd $HOME/.config/OpenRGB && openrgb &)
(corectrl &)
(syncthing-gtk &)
