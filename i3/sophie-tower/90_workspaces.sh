#!/usr/bin/env bash

source $DOTFILES_HOST/xoutputs.sh

i3-msg "workspace number \"$ws1\"; move workspace to output $display_left"
i3-msg "workspace number \"$ws2\"; move workspace to output $display_right"
i3-msg "workspace number \"$ws3\"; move workspace to output $display_left"
i3-msg "workspace number \"$ws4\"; move workspace to output $display_left"
