# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
	PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ] ; then
	PATH="$HOME/.local/bin:$PATH"
fi

# add cargo to PATH
if [ -d "$HOME/.cargo/bin" ] ; then
	PATH="$HOME/.cargo/bin:$PATH"
fi

# add dotnet tools to PATH
if [ -d "$HOME/.dotnet/tools" ] ; then
	PATH="$HOME/.dotnet/tools:$PATH"
fi

# use desktop portal if available
# requires `xdg-desktop-portal` package in Arch and one implementation
if [ -f "/usr/share/dbus-1/services/org.freedesktop.portal.Desktop.service" ]; then
	export GTK_USE_PORTAL=1
fi

# start gnome keyring
if hash gnome-keyring-daemon 2>/dev/null; then
	eval $(gnome-keyring-daemon --start)
	export SSH_AUTH_SOCK
fi

# set xdg defaults
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DATA_DIRS="/usr/local/share:/usr/share"
export XDG_CONFIG_DIRS="/etc/xdg"

# set locale stuff
export LANG=en_US.UTF-8
export LC_TIME=en_DK.UTF-8 # ISO8601
export LC_MONETARY=de_DE.UTF-8
export LC_MEASUREMENT=de_DE.UTF-8
export LC_NUMERIC=de_DE.UTF-8
export LC_PAPER=de_DE.UTF-8
export PAPERSIZE=a4

source ~/.zshrc_dotfiles
export DOTFILES
PATH="$DOTFILES/bin:$PATH"

# source hostname-specific profile if exists
export DOTFILES_HOST="$DOTFILES/host/$(uname -n)"
if [ -f "$DOTFILES_HOST/profile" ]; then
	source "$DOTFILES_HOST/profile"
fi

# source local profile if exists
if [ -f "$HOME/.profile.local" ] ; then
	source ~/.profile.local
fi

# use wayland for firefox and sdl
export MOZ_ENABLE_WAYLAND=1
export SDL_VIDEODRIVER=wayland

# theming
if [ -z "$QT_QPA_PLATFORMTHEME" ]; then
	# qt6ct is used for qt6 applications even if qt5ct is specified. amazing.
	# alternatively force style:
	#export QT_STYLE_OVERRIDE=kvantum
	export QT_QPA_PLATFORMTHEME=qt5ct
fi
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtkrc-2.0"

# anti-aliased fonts
export JDK_JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aattext=true'
# use OpenGL based hardware acceleration
export JDK_JAVA_OPTIONS="$JDK_JAVA_OPTIONS -Dsun.java2d.opengl=true"
# use GTK look
export JDK_JAVA_OPTIONS="$JDK_JAVA_OPTIONS -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel"

# dotnet telemetry opt-out
export DOTNET_CLI_TELEMETRY_OPTOUT=1

# set java root dir (defaults to ~/.java)
export JAVA_TOOL_OPTIONS="-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java"

dbus-update-activation-environment --systemd --all
