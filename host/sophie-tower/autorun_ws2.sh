#!/usr/bin/env bash

source $DOTFILES/sway/workspaces

evolution_id=0
discord_id=0
signal_id=0
element_id=0

discord_count=0
open_count=0

function load_layout {
	swaymsg "[con_id=$evolution_id] move to workspace number $ws2; [workspace=$ws2] layout tabbed"
	swaymsg "[con_id=$element_id] move to workspace number $ws2; [con_id=$element_id] split horizontal"
	swaymsg "[con_id=$signal_id] move to workspace number $ws2; [con_id=$signal_id] move right; [con_id=$signal_id] split vertical"
	swaymsg "[con_id=$discord_id] move to workspace number $ws2; [con_id=$discord_id] move right; [con_id=$discord_id] move left"
}

sh -c '(sleep 2; (evolution &); (element-desktop &); (signal-desktop &); (discord &))' &

swaymsg -m -t SUBSCRIBE '["window"]' | \
jq --unbuffered --compact-output 'select(.change | contains("new")) | .container' | \
while read -r container; do
	app_id=$(echo "$container" | jq -r '.app_id')
	id=$(echo "$container" | jq -r '.id')
	x11=$(echo "$container" | jq '.window_properties')
	class=$(echo "$container" | jq -r '.window_properties.class')
	instance=$(echo "$container" | jq -r '.window_properties.instance')

	echo "window $id opened with app id $app_id / class $class"
	case "$app_id" in
		Element)
			echo "element open: $id"
			element_id=$id
			open_count=$(( $open_count + 1 ))
			;;
		evolution)
			echo "evolution open: $id"
			evolution_id=$id
			open_count=$(( $open_count + 1 ))
			;;
		*)
			case "$class" in
				discord)
					if [ $discord_count = 0 ]; then
						echo "discord loading screen: $id"
						discord_count=$(( $discord_count + 1 ))
					else
						echo "discord open: $id"
						discord_id=$id
						open_count=$(( $open_count + 1 ))
					fi
					;;
				Signal)
					echo "signal open: $id"
					signal_id=$id
					open_count=$(( $open_count + 1 ))
					;;
			esac
			;;
	esac

	if [ $open_count = 4 ]; then
		echo "everything open as required, loading layout"
		load_layout
		exit 0
	fi
done
