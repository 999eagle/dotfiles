#!/usr/bin/env bash

display_index=DP-1
display_left=DP-2
display_right=HDMI-A-1

swaymsg "output $display_left  mode 1920x1080@165Hz pos 0    0 adaptive_sync on"
swaymsg "output $display_right mode 1920x1080       pos 1920 0"
