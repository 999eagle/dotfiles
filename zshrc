#!/usr/bin/env zsh

# Get the path to the dotfiles repo
source $HOME/.zshrc_dotfiles
export DOTFILES

# Start X11 if we don't have a display, are on a tty and have a linux terminal
if [ -z "$DISPLAY" ] && [ "$XDG_SESSION_TYPE" = "tty" ] && [ "$TERM" = "linux" ] && [ "$XDG_VTNR" -le 3 ]; then
	if which sway >/dev/null 2>&1; then
		exec sway
	elif which startx >/dev/null 2>&1; then
		exec startx
	else
		: ${ZSH_TMUX_AUTOSTART:=0}
		export ZSH_TMUX_AUTOSTART
	fi
fi

# Load tmux
source $DOTFILES/rc/tmux/tmux.zsh

export PATH="$DOTFILES/local/bin:$PATH"

# enable extended globbing
setopt extended_glob

# source fonts
source $DOTFILES/rc/config.zsh
for f in $DOTFILES/awesome-terminal-fonts/build/*.sh; do
	source $f
done
source $DOTFILES/local/bin/nerd-fonts/i_all.sh

# Plugin configuration
export ZSH_PLUGINS_ALIAS_TIPS_EXCLUDES="_ g"
DISABLE_AUTO_UPDATE=true
ZSH="$(antibody home)/https-COLON--SLASH--SLASH-github.com-SLASH-robbyrussell-SLASH-oh-my-zsh/"
source $DOTFILES/rc/p9k_preconfig.zsh

# Set history options
export HISTFILE=$DOTFILES/local/zsh_history

# Load antibody plugins
source $DOTFILES/local/antibody_plugins.zsh
# Load P9K configuration after P9K to have utility functions
source $DOTFILES/rc/p9k_config.zsh
# Completion for kitty
if which kitty >/dev/null 2>&1; then
	kitty + complete setup zsh | source /dev/stdin
fi
# Load aliases
source $HOME/.aliases

# SSH agent configuration
if [[ -z "${SSH_AUTH_SOCK}" && -S "${XDG_RUNTIME_DIR}/ssh-agent.sock" ]]; then
	export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh-agent.sock"
	if [[ -n "${TMUX}" ]]; then
		tmux set-env SSH_AUTH_SOCK "$SSH_AUTH_SOCK"
	fi
fi
